Source: golang-github-minio-highwayhash
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Dominik George <natureshadow@debian.org>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-golang-x-sys-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-minio-highwayhash
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-minio-highwayhash.git
Homepage: https://github.com/minio/highwayhash
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/minio/highwayhash

Package: golang-github-minio-highwayhash-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-golang-x-sys-dev,
         ${misc:Depends}
Description: Native Go implementation of HighwayHash (library)
 This library is a native Go version of HighwayHash with optimized assembly
 implementations on Intel and ARM, Able to process over 10 GB/sec on a single
 core on Intel CPUs.
 .
 HighwayHash is a pseudo-random function (PRF) developed by Jyrki Alakuijala,
 Bill Cox and Jan Wassenberg (Google research). HighwayHash takes a 256 bit key
 and computes 64, 128 or 256 bit hash values of given messages.
 .
 It can be used to prevent hash-flooding attacks or authenticate short-lived
 messages. Additionally it can be used as a fingerprinting
 function. HighwayHash is not a general purpose cryptographic hash
 function (such as Blake2b, SHA-3 or SHA-2) and should not be used if
 strong collision resistance is required.
